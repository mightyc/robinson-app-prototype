//
//  ClubCell.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 13/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "ClubCell.h"

@implementation ClubCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
