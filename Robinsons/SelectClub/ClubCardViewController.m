//
//  ClubCardViewController.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 13/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "ClubCardViewController.h"
#import "ZLSwipeableView.h"
#import "ClubCard.h"
#import "ClubCell.h"
#import "ClubDetailViewController.h"
@interface ClubCardViewController ()
@property (nonatomic, retain) NSArray *clubImages;
@property (nonatomic) NSUInteger colorIndex;
@property (nonatomic, retain) NSArray *clubNames;
@property (nonatomic, retain) NSArray *clubIds;
@property (nonatomic, retain) NSArray *productCodes;

@property (nonatomic, retain) NSArray *summerClubNames;
@property (nonatomic, retain) NSArray *summerClubIds;
@property (nonatomic, retain) NSArray *summerClubProductCodes;
@property (nonatomic, retain) NSArray *summerClubImages;

@property (nonatomic, retain) NSArray *winterClubNames;
@property (nonatomic, retain) NSArray *winterClubIds;
@property (nonatomic, retain) NSArray *winterClubProductCodes;
@property (nonatomic, retain) NSArray *winterClubImages;


@end

@implementation ClubCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.colorIndex = 0;
    
//    self.productCodes = @[
//                          1@"KLU25062",
//                          2@"FAO26026",
//                          3@"VOA42061",
//                          4@"ZDT69061",
//                          5@"KLU58071",
//                          6@"DJE19019",
//                          7@"AGP70001",
//                          8@"AGA11055",
//                          9@"AYT46015",
//                          10@"KGS11510",
//                          11@"KGS15010",
//                          12@"SZG87041",
//                          13@"LNZ42061",
//                          14@"SZG87041",
//                          15@"AYT12050",
//                          16@"HRG15025",
//                          17@"ZDT57061",
//                          18@"DLM15030",
//                          19@"MLE12120",
//                          20@"PMI55023",
//                          21@"AYT31043",
//                          22@"MVP53015",
//                          23@"HER23060",
//                          24@"BDS15010",
//                          25@"FUE11001"
//                          ];
//    self.clubIds = @[
//                     1@"23",
//                     2@"14",
//                     3@"5",
//                     4@"15",
//                     5@"21",
//                     6@"25",
//                     7@"16",
//                     8@"3",
//                     9@"11",
//                     10@"26",
//                     11@"8",
//                     12@"1",
//                     13@"9",
//                     14@"10",
//                     15@"18",
//                     16@"2",
//                     17@"17",
//                     18@"19",
//                     19@"7",
//                     20@"12",
//                     21@"13",
//                     22@"4",
//                     23@"24",
//                     24@"22",
//                     25@"6"
//                     ];
//    self.clubImages = @[
//                    1@"Landskron",
//                    2@"Quinta",
//                    3@"Alpenrose",
//                    4@"Schweizerhof",
//                    5@"Schlanitzen",
//                    6@"Touristique",
//                    7@"Granada",
//                    8@"Agadir",
//                    9@"Pamfilya",
//                    10@"Cruise",
//                    11@"Daidalos",
//                    12@"Playa",
//                    13@"Ampflwang",
//                    14@"Amade",
//                    15@"Camyuva",
//                    16@"Soma",
//                    17@"Arosa",
//                    18@"Sarigerme",
//                    19@"Maldives",
//                    20@"Serena",
//                    21@"Nobilis",
//                    22@"Fleesensee",
//                    23@"Kriti",
//                    24@"Apulia",
//                    25@"Jandia"
//                    ];
//    
//    self.clubNames = @[
//                       1@"Club Landskron",
//                       2@"Club Quinta da Ria",
//                       3@"Club Alpenrose Zürs",
//                       4@"Club Schweizerhof",
//                       5@"Club Schlanitzen Alm",
//                       6@"Zone Touristique Ennadhour",
//                       7@"Club Playa Granada",
//                       8@"Club Agadir",
//                       9@"Club Pamfilya",
//                       10@"ROBINSON Cruise SC Panorama",
//                       11@"Club Daidalos",
//                       12@"Club Esquinzo Playa",
//                       13@"Club Ampflwang",
//                       14@"Club Amade",
//                       15@"Club Camyuva",
//                       16@"Club Soma Bay",
//                       17@"Club Arosa",
//                       18@"Club Sarigerme Park",
//                       19@"Club Maldives",
//                       20@"Club Cala Serena",
//                       21@"Club Nobilis",
//                       22@"Club Fleesensee",
//                       23@"Club Kalimera Kriti",
//                       24@"Club Apulia",
//                       25@"Club Jandia Playa"
//                       ];
    
    self.summerClubNames = @[
                       @"Club Landskron",
                       @"Club Quinta da Ria",
                       @"Club Schweizerhof",
                       @"Club Schlanitzen Alm",
                       @"Club Playa Granada",
                       @"Club Agadir",
                       @"Club Pamfilya",
                       @"Club Daidalos",
                       @"Club Esquinzo Playa",
                       @"Club Ampflwang",
                       @"Club Amade",
                       @"Club Camyuva",
                       @"Club Soma Bay",
                       @"Club Sarigerme Park",
                       @"Club Maldives",
                       @"Club Cala Serena",
                       @"Club Nobilis",
                       @"Club Fleesensee",
                       @"Club Kalimera Kriti",
                       @"Club Apulia",
                       @"Club Jandia Playa"
                       ];
    self.summerClubImages = @[
                        @"Landskron",
                        @"Quinta",
                        @"Schweizerhof",
                        @"Schlanitzen",
                        @"Granada",
                        @"Agadir",
                        @"Pamfilya",
                        @"Daidalos",
                        @"Playa",
                        @"Ampflwang",
                        @"Amade",
                        @"Camyuva",
                        @"Soma",
                        @"Sarigerme",
                        @"Maldives",
                        @"Serena",
                        @"Nobilis",
                        @"Fleesensee",
                        @"Kriti",
                        @"Apulia",
                        @"Jandia"
                        ];
    
    self.summerClubIds = @[
                     @"23",
                     @"14",
                     @"15",
                     @"21",
                     @"16",
                     @"3",
                     @"11",
                     @"8",
                     @"1",
                     @"9",
                     @"10",
                     @"18",
                     @"2",
                     @"19",
                     @"7",
                     @"12",
                     @"13",
                     @"4",
                     @"24",
                     @"22",
                     @"6"
                     ];
    
    self.summerClubProductCodes = @[
                          @"KLU25062",
                          @"FAO26026",
                          @"ZDT69061",
                          @"KLU58071",
                          @"AGP70001",
                          @"AGA11055",
                          @"AYT46015",
                          @"KGS15010",
                          @"SZG87041",
                          @"LNZ42061",
                          @"SZG87041",
                          @"AYT12050",
                          @"HRG15025",
                          @"DLM15030",
                          @"MLE12120",
                          @"PMI55023",
                          @"AYT31043",
                          @"MVP53015",
                          @"HER23060",
                          @"BDS15010",
                          @"FUE11001"
                          ];
    
    //================SUMMER COMPLETED
    
    
    self.winterClubProductCodes = @[
                          @"KLU25062",
                          @"FAO26026",
                          @"VOA42061",
                          @"ZDT69061",
                          @"KLU58071",
                          @"AGA11055",
                          @"SZG87041",
                          @"LNZ42061",
                          @"SZG87041",
                          @"HRG15025",
                          @"ZDT57061",
                          @"MLE12120",
                          @"PMI55023",
                          @"AYT31043",
                          @"MVP53015",
                          @"FUE11001"
                          ];

    self.winterClubIds = @[
                     @"23",
                     @"14",
                     @"5",
                     @"15",
                     @"21",
                     @"3",
                     @"1",
                     @"9",
                     @"10",
                     @"2",
                     @"17",
                     @"7",
                     @"12",
                     @"13",
                     @"4",
                     @"6"
                     ];
    self.winterClubImages = @[
                        @"Landskron",
                        @"Quinta",
                        @"Alpenrose",
                        @"Schweizerhof",
                        @"Schlanitzen",
                        @"Agadir",
                        @"Playa",
                        @"Ampflwang",
                        @"Amade",
                        @"Soma",
                        @"Arosa",
                        @"Maldives",
                        @"Serena",
                        @"Nobilis",
                        @"Fleesensee",
                        @"Jandia"
                        ];
    
    self.winterClubNames = @[
                       @"Club Landskron",
                       @"Club Quinta da Ria",
                       @"Club Alpenrose Zürs",
                       @"Club Schweizerhof",
                       @"Club Schlanitzen Alm",
                       @"Club Agadir",
                       @"Club Esquinzo Playa",
                       @"Club Ampflwang",
                       @"Club Amade",
                       @"Club Soma Bay",
                       @"Club Arosa",
                       @"Club Maldives",
                       @"Club Cala Serena",
                       @"Club Nobilis",
                       @"Club Fleesensee",
                       @"Club Jandia Playa"
                       ];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSections{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([self.seasonType isEqualToString:@"winter"]) {
        return [self.winterClubProductCodes count];
    }
    else
        return [self.summerClubProductCodes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cellIdentifier = @"ClubCell";
    ClubCell  *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil) {
        
        NSArray *xibViews = [[NSBundle mainBundle] loadNibNamed:@"ClubCell" owner:nil options:nil];
        cell = (ClubCell*)[xibViews lastObject];
        if([self.seasonType isEqualToString:@"winter"])
        {
            NSString *clubImageString = [NSString stringWithFormat:@"%@.jpg",[self.winterClubImages objectAtIndex:indexPath.row]];
            cell.clubImage.image = [UIImage imageNamed:clubImageString];
            cell.clubName.text = [self.winterClubNames objectAtIndex:indexPath.row];
        }
        else{
            NSString *clubImageString = [NSString stringWithFormat:@"%@.jpg",[self.summerClubImages objectAtIndex:indexPath.row]];
            cell.clubImage.image = [UIImage imageNamed:clubImageString];
            cell.clubName.text = [self.summerClubNames objectAtIndex:indexPath.row];
        }
        
            
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 384;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ClubDetailViewController* viewController = [[ClubDetailViewController alloc] init];
    if([self.seasonType isEqualToString:@"winter"]){
        viewController.clubNameString = [[self.winterClubNames objectAtIndex:indexPath.row] uppercaseString];
        viewController.clubImageNameString = [self.winterClubImages objectAtIndex:indexPath.row];
        viewController.clubId = [self.winterClubIds objectAtIndex:indexPath.row];
        viewController.productCode = [self.winterClubProductCodes objectAtIndex:indexPath.row];
        viewController.seasonId = self.seasonId;
        viewController.startDate = self.startDate;
        viewController.endDate = self.endDate;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else{
        viewController.clubNameString = [[self.summerClubNames objectAtIndex:indexPath.row] uppercaseString];
        viewController.clubImageNameString = [self.summerClubImages objectAtIndex:indexPath.row];
        viewController.clubId = [self.summerClubIds objectAtIndex:indexPath.row];
        viewController.productCode = [self.summerClubProductCodes objectAtIndex:indexPath.row];
        viewController.seasonId = self.seasonId;
        viewController.startDate = self.startDate;
        viewController.endDate = self.endDate;
        [self.navigationController pushViewController:viewController animated:YES];
    }

}


@end
