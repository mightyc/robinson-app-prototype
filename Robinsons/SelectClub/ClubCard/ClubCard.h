//
//  ClubCard.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 13/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClubCard : UIView

@property UIColor *cardColor;
@property (nonatomic, retain) UIImageView *clubImage;
@end
