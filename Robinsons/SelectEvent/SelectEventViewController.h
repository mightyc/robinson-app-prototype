//
//  SelectEventViewController.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 26/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectEventViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSArray *eventsCategoryArray;
//    NSArray *eventsCategoryIds;
//    NSArray *eventsCategoryImages;
    
    int checkForSorting;
}
@property(nonatomic, retain) NSString *seasonType;
@property (nonatomic, retain) NSString *seasonId;

@property (nonatomic, retain) NSString *startDate;
@property (nonatomic, retain) NSString *endDate;

@property (weak, nonatomic) IBOutlet UITableView *eventsTable;
@end
