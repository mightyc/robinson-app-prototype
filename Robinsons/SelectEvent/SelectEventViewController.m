//
//  SelectEventViewController.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 26/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "SelectEventViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "UIImageView+AFNetworking.h"
#import "ClubCell.h"
#import "EventsByCategoryViewController.h"
#import "CategoryWithEvent.h"
@interface SelectEventViewController ()

@end

@implementation SelectEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    checkForSorting = 0;
    
    CategoryWithEvent *event1 = [[CategoryWithEvent alloc] init];
    event1.categoryId = @"34";
    event1.categoryName = @"Adventure";
    event1.categoryImageName = @"Adventure";
    
    CategoryWithEvent *event2 = [[CategoryWithEvent alloc] init];
    event2.categoryId = @"12";
    event2.categoryName = @"Beachvolleyball";
    event2.categoryImageName = @"Beachvolleyball";
    
    CategoryWithEvent *event3 = [[CategoryWithEvent alloc] init];
    event3.categoryId = @"33";
    event3.categoryName = @"Biken";
    event3.categoryImageName = @"Biken";
    
    CategoryWithEvent *event4 = [[CategoryWithEvent alloc] init];
    event4.categoryId = @"29";
    event4.categoryName = @"Boxen";
    event4.categoryImageName = @"Boxen";

    CategoryWithEvent *event5 = [[CategoryWithEvent alloc] init];
    event5.categoryId = @"24";
    event5.categoryName = @"Entertainments";
    event5.categoryImageName = @"Entertainments";

    CategoryWithEvent *event6 = [[CategoryWithEvent alloc] init];
    event6.categoryId = @"21";
    event6.categoryName = @"Essen & Trinken";
    event6.categoryImageName = @"Essen";

    CategoryWithEvent *event7 = [[CategoryWithEvent alloc] init];
    event7.categoryId = @"1";
    event7.categoryName = @"Golf";
    event7.categoryImageName = @"Golf";

    CategoryWithEvent *event8 = [[CategoryWithEvent alloc] init];
    event8.categoryId = @"31";
    event8.categoryName = @"Kinder & Jugendliche";
    event8.categoryImageName = @"Kinder";

    CategoryWithEvent *event9 = [[CategoryWithEvent alloc] init];
    event9.categoryId = @"20";
    event9.categoryName = @"Laufen";
    event9.categoryImageName = @"Laufen";

    CategoryWithEvent *event10 = [[CategoryWithEvent alloc] init];
    event10.categoryId = @"15";
    event10.categoryName = @"Lifestyle & Atelier";
    event10.categoryImageName = @"Lifestyle";

    CategoryWithEvent *event11 = [[CategoryWithEvent alloc] init];
    event11.categoryId = @"30";
    event11.categoryName = @"Sport Extra";
    event11.categoryImageName = @"Sport";
    
    CategoryWithEvent *event12 = [[CategoryWithEvent alloc] init];
    event12.categoryId = @"14";
    event12.categoryName = @"Tennis";
    event12.categoryImageName = @"Tennis";

    CategoryWithEvent *event13 = [[CategoryWithEvent alloc] init];
    event13.categoryId = @"28";
    event13.categoryName = @"Tischtennis";
    event13.categoryImageName = @"Tischtennis";

    CategoryWithEvent *event14 = [[CategoryWithEvent alloc] init];
    event14.categoryId = @"35";
    event14.categoryName = @"Triathlon";
    event14.categoryImageName = @"Triathlon";
    

    CategoryWithEvent *event15 = [[CategoryWithEvent alloc] init];
    event15.categoryId = @"19";
    event15.categoryName = @"Wandern";
    event15.categoryImageName = @"Wandern";

    CategoryWithEvent *event16 = [[CategoryWithEvent alloc] init];
    event16.categoryId = @"9";
    event16.categoryName = @"Wassersport";
    event16.categoryImageName = @"Wassersport";
    
    CategoryWithEvent *event17 = [[CategoryWithEvent alloc] init];
    event17.categoryId = @"10";
    event17.categoryName = @"WellFit®";
    event17.categoryImageName = @"Wellfit";

    CategoryWithEvent *event18 = [[CategoryWithEvent alloc] init];
    event18.categoryId = @"17";
    event18.categoryName = @"Wintersport";
    event18.categoryImageName = @"Wintersport";
    
    
    
    eventsCategoryArray = @[
                            event1,
                            event2,
                            event3,
                            event4,
                            event5,
                            event6,
                            event7,
                            event8,
                            event9,
                            event10,
                            event11,
                            event12,
                            event13,
                            event14,
                            event15,
                            event16,
                            event17,
                            event18,
                          ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)numberOfSections{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return eventsCategoryArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cellIdentifier = @"ClubCell";
    ClubCell  *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil) {
        
        NSArray *xibViews = [[NSBundle mainBundle] loadNibNamed:@"ClubCell" owner:nil options:nil];
        
        CategoryWithEvent *event = [[CategoryWithEvent alloc] init];
        event = [eventsCategoryArray objectAtIndex:indexPath.row];
        cell = (ClubCell*)[xibViews lastObject];
        NSString *clubImageString = [NSString stringWithFormat:@"%@.jpg",event.categoryImageName];
        cell.clubImage.image = [UIImage imageNamed:clubImageString];
        cell.clubName.text = event.categoryName;
        
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 384;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CategoryWithEvent *event = [eventsCategoryArray objectAtIndex:indexPath.row];
    EventsByCategoryViewController* viewController = [[EventsByCategoryViewController alloc] init];
    viewController.categoryIdsList = event.categoryId;
    viewController.startDate = self.startDate;
    viewController.endDate = self.endDate;
    [self.navigationController pushViewController:viewController animated:YES];
//
}

- (IBAction)nameSorting:(id)sender {
    
    NSArray *unsortedList = [eventsCategoryArray copy];
    NSSortDescriptor *nameDescriptor;
    if(checkForSorting == 0){
        nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"categoryName" ascending:NO];
        checkForSorting = 1;
    }
    else{
        nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"categoryName" ascending:YES];
        checkForSorting = 0;
    }
    NSArray *sortDescriptors = @[nameDescriptor];
    eventsCategoryArray = [eventsCategoryArray sortedArrayUsingDescriptors:sortDescriptors];
        
    [self.eventsTable beginUpdates];
    
    // Move the cells around
    NSInteger sourceRow = 0;
    for (CategoryWithEvent *event in unsortedList) {
        NSInteger destRow = [eventsCategoryArray indexOfObject:event];
        
        if (destRow != sourceRow) {
            // Move the rows within the table view
            NSIndexPath *sourceIndexPath = [NSIndexPath indexPathForItem:sourceRow inSection:0];
            NSIndexPath *destIndexPath = [NSIndexPath indexPathForItem:destRow inSection:0];
            [self.eventsTable moveRowAtIndexPath:sourceIndexPath toIndexPath:destIndexPath];
            
        }
        sourceRow++;
    }
    
    // Commit animations
    [self.eventsTable endUpdates];
}



@end
