//  Created by SAROSH

#import <UIKit/UIKit.h>
// using iPhone 6 res for now
/*
 when doing the resolution resampling please go through this
 http://www.paintcodeapp.com/news/iphone-6-screens-demystified
 very informative
 */
#define kDialogHeight    750.0  // height of the Inner view, we draw a layer arround the view
#define kDialogWidth     1334.0  // width of the inner view, we draw a layer arround the view
#define kInnerMargin       5.0  // margin of the layer
#define kQuitButtonHeight 50.0  // quit button frame height, for the touchable area
#define kQuitButtonWidth  50.0  // quit button frame witdh, for the touchable area
#define kDuration            0.50 // effect duration, the bounce effect, and dismiss effect depend on this

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*!
 @class      DialogView
 @abstract   This class handles an Dialog
 @discussion This class is the base implementation to handle the Qype Dialog to add some specific beheaviour extend the 
 class. This class provides override points to add your beheaviour.
 */
@interface DialogView : UIView {
@private
	CGSize    mInnerSize;
	UIStatusBarStyle mOldStyle;
    NSTimer *timer;
	
@protected
	UIButton *mQuitButton;
	UIView   *mBackgroundView;	
	CGFloat   mBackGroundColor[4];
}

@property (nonatomic, readonly) CGSize  innerSize; // returns the size of the usable view
@property (nonatomic, readonly) CGFloat margin;    // returns the margin of the layer
@property (nonatomic, retain) IBOutlet UIView *containerView;
@property (nonatomic, retain) IBOutlet UILabel *timeLabel;

- (void) initializeSettings;

- (void)show;

- (void)load;

- (void)dialogWillAppear;

- (void)cancel;

- (void)dismissView:(BOOL)animated;

- (NSString *) currentDateString;

@end