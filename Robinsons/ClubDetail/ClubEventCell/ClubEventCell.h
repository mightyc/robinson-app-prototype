//
//  ClubEventCell.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 09/02/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClubEventCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *eventPrice;
@property (weak, nonatomic) IBOutlet UILabel *eventName;
@property (weak, nonatomic) IBOutlet UIImageView *eventImage;
@property (weak, nonatomic) IBOutlet UIImageView *clubImage;

@property (weak, nonatomic) IBOutlet UIView *priceView;

@property (weak, nonatomic) IBOutlet UIWebView *eventLongText;
@property (weak, nonatomic) IBOutlet UIImageView *flag_image;
@property (weak, nonatomic) IBOutlet UIButton *countryButton;
@property (nonatomic,retain) NSString *countryName;

@property (nonatomic, retain) NSString *shareUrlString;

@end
