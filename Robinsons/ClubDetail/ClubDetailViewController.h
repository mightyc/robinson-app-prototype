//
//  ClubDetailViewController.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 15/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZLSwipeableView.h"
#import "UIColor+FlatColors.h"
#import "Club.h"
#import "Event.h"
@interface ClubDetailViewController : UIViewController<UIScrollViewDelegate, ZLSwipeableViewDataSource, ZLSwipeableViewDelegate,UITableViewDataSource, UITableViewDelegate>
{
    NSString *imgPath;
    NSString *mediaImagePath;
    NSString *clubDetailUrlString;
    
    NSString *eventsByClubUrlString;
    NSString *clubOfferUrl;
    
    NSString *robinsonUrl;
    NSString *sharebleClubDetailUrl;
    __weak IBOutlet UIButton *shareClubButton;
    
    
    __weak IBOutlet UIActivityIndicatorView *webViewContentLoader;
    
    
    __weak IBOutlet UIWebView *iconHolidayCheckWeb;
    
    __weak IBOutlet UIWebView *widgetHolidayCheck;
    
    
    
    
    
    
    __weak IBOutlet UILabel *arrival_longLabel;
    __weak IBOutlet UILabel *transferLabel;
    
    NSMutableArray *eventsArray;
    
}
@property (weak, nonatomic) IBOutlet UITableView *eventsTable;
@property (weak, nonatomic) IBOutlet UILabel *clubPrice;
@property (weak, nonatomic) IBOutlet UILabel *clubPersonType;
@property (weak, nonatomic) IBOutlet UILabel *clubRoomType;
@property (nonatomic, retain) NSString *startDate;
@property (nonatomic, retain) NSString *endDate;

@property (nonatomic, retain) NSString *clubId;
@property (nonatomic, retain) NSString *seasonId;
@property (nonatomic, retain) NSString *productCode;
@property (nonatomic, retain) Club *club;
@property (nonatomic, retain) Event *event;


@property (weak, nonatomic) IBOutlet ZLSwipeableView *swipeableView;
@property (nonatomic) NSUInteger colorIndex;
@property (nonatomic, retain) NSArray *clubImages;

@property (nonatomic, retain) NSString *clubImageNameString;

@property (weak, nonatomic) IBOutlet UIButton *overViewButton;
@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@property (weak, nonatomic) IBOutlet UIButton *eventsButton;
@property (weak, nonatomic) IBOutlet UIButton *essenButton;





@property (weak, nonatomic) IBOutlet UILabel *clubName;
@property (nonatomic, retain) NSString *clubNameString;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;


@property (weak, nonatomic) IBOutlet UIView *clubOverviewView;
@property (weak, nonatomic) IBOutlet UIImageView *clubOverviewImage;
@property (weak, nonatomic) IBOutlet UIWebView *webView;


@property (weak, nonatomic) IBOutlet UIView *clubDetailView;
@property (weak, nonatomic) IBOutlet UIImageView *clubDetailImage;

@property (weak, nonatomic) IBOutlet UIView *clubEventsView;
@property (weak, nonatomic) IBOutlet UIImageView *clubEventImage;


@property (weak, nonatomic) IBOutlet UIView *clubEssenView;
@property (weak, nonatomic) IBOutlet UIImageView *clubEssenImage;
@property (weak, nonatomic) IBOutlet UIWebView *eatAndDrinkLeftColumnWebView;
@property (weak, nonatomic) IBOutlet UIWebView *eatAndDrinkRightColumnWebView;
@property (weak, nonatomic) IBOutlet UILabel *eatAndDrinkTitle;




@end
