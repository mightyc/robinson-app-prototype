//
//  ClubDetailViewController.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 15/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "ClubDetailViewController.h"
#import "ClubGallery.h"
#import <AFNetworking/AFNetworking.h>
#import "UIImageView+AFNetworking.h"
#import "Image.h"
#import <FacebookSDK/FacebookSDK.h>
#import "EventCell.h"
#import "WikiPopUp.h"
#import "ClubEventCell.h"
@interface ClubDetailViewController ()

@end

@implementation ClubDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    //robinson.stage.mtc.fcse.io
    
    NSLog(@"CLUB ID: %@",self.clubId);
    NSLog(@"SEASON ID: %@",self.seasonId);
    
    NSLog(@"START DATE: %@",self.startDate);
    NSLog(@"RETURN DATE: %@",self.endDate);

    imgPath = @"http://media-index.robinson.plusline.de/";
    mediaImagePath = @"http://media-index.robinson.plusline.de/clubevents/";
    robinsonUrl = @"http://www.robinson.com/de/de/";
    
    
    clubDetailUrlString = [NSString stringWithFormat:@"http://app:IToor8ah@robinson.stage.mtc.fcse.io/api/seasons/de_DE/%@/%@?_format=json&size=30&from=0",self.clubId,self.seasonId];
    
    eventsByClubUrlString = [NSString stringWithFormat:@"http://app:IToor8ah@robinson.stage.mtc.fcse.io/api/events/de_DE/product_codes/%@?_format=json&startDate=%@&endDate=%@&isTopEvent=true&size=30&from=0",self.productCode,self.startDate,self.endDate];
    
    [webViewContentLoader startAnimating];
    
    NSLog(@"URL REQUEST: %@",clubDetailUrlString);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:clubDetailUrlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [webViewContentLoader stopAnimating];
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        data = [responseObject objectAtIndex:0];
        self.club = [[Club alloc] initWithDictionary:data];
        NSLog(@"CLUB DETAIL SHAREABLE URL: %@",self.club.season_club_detail_url);
        sharebleClubDetailUrl = [NSString stringWithFormat:@"%@%@",robinsonUrl,self.club.season_club_detail_url];
        [shareClubButton setHidden:NO];
        // loading long intro
        NSString *clubIntro = self.club.clubInfo.intro_long;
        [self.webView loadHTMLString:clubIntro baseURL:nil];
        
        
        //loading Essen text
        
        [self.eatAndDrinkLeftColumnWebView loadHTMLString:self.club.clubInfo.eatAndDrink.left_column baseURL:nil];
        [self.eatAndDrinkRightColumnWebView loadHTMLString:self.club.clubInfo.eatAndDrink.right_column baseURL:nil];
        self.eatAndDrinkTitle.text = self.club.clubInfo.eatAndDrink.link_text;
        
        [iconHolidayCheckWeb loadHTMLString:self.club.clubInfo.icon_holidayCheck baseURL:nil];
        [widgetHolidayCheck loadHTMLString:self.club.clubInfo.widget_holidayCheck baseURL:nil];
        
        
        arrival_longLabel.text = self.club.clubInfo.arrival_long;
        transferLabel.text = self.club.clubInfo.transfer;
        
        Image *sampleImage = [self.club.clubInfo.gallery.images objectAtIndex:0];
        NSLog(@"IMAGES URL: %@",sampleImage.originalName);

        [self loadGalleryImages];
        
        
        
        
        
        /*
         At completion of previous call load the events call
         */
        
        
        NSLog(@"EVENTS CALL: %@",eventsByClubUrlString);
        AFHTTPRequestOperationManager *eventsManager = [AFHTTPRequestOperationManager manager];
        [eventsManager GET:eventsByClubUrlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            /*
             At completion of previous call load the events call
             */
            eventsArray = [[NSMutableArray alloc] init];
            NSMutableArray *temporaryEventsArray = [[NSMutableArray alloc] init];
            temporaryEventsArray = responseObject;
            for (int i = 0; i<temporaryEventsArray.count; i++) {
                Event *eventObject = [[Event alloc] initWithDictionary:[temporaryEventsArray objectAtIndex:i]];
                [eventsArray insertObject:eventObject atIndex:i];
                [self.eventsTable reloadData];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
        
        
        clubOfferUrl = [NSString stringWithFormat:@"http://robinson.com/de/de/siteAjax/ClubOffer?productCodes=%@&departureDate=%@&returnDate=%@&hotelOnly=0",self.productCode,self.startDate,self.endDate];

        
        AFHTTPRequestOperationManager *clubOfferManager = [AFHTTPRequestOperationManager manager];
        [clubOfferManager GET:clubOfferUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            /*
             At completion of previous call load the events call
             */
            NSMutableDictionary *clubOfferObject = [[NSMutableDictionary alloc] init];
            clubOfferObject = responseObject;
            self.clubPrice.text  = [NSString stringWithFormat:@"%@ €",[clubOfferObject objectForKey:@"rawPrice"]];
            self.clubPersonType.text = [clubOfferObject objectForKey:@"person_text"];
            self.clubRoomType.text = [clubOfferObject objectForKey:@"info_text"];
            
            if ([clubOfferObject objectForKey:@"rawPrice"] != nil) {
                [self.clubPrice setHidden:NO];
                [self.clubPersonType setHidden:NO];
                [self.clubRoomType setHidden:NO];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    //making webviews transparent
    iconHolidayCheckWeb.opaque = NO;
    iconHolidayCheckWeb.backgroundColor = [UIColor clearColor];

    widgetHolidayCheck.opaque = NO;
    widgetHolidayCheck.backgroundColor = [UIColor clearColor];
    
    
    
    self.webView.opaque = NO;
    self.webView.backgroundColor = [UIColor clearColor];
    
    self.eatAndDrinkLeftColumnWebView.opaque = NO;
    self.eatAndDrinkLeftColumnWebView.backgroundColor = [UIColor clearColor];
    self.eatAndDrinkRightColumnWebView.opaque = NO;
    self.eatAndDrinkRightColumnWebView.backgroundColor = [UIColor clearColor];

    
    
    self.colorIndex = 0;
    self.clubImages = @[
                        @"Landskron1",
                        @"Landskron2",
                        @"Landskron3",
                        @"Landskron4",
                        @"Soma1",
                        @"Soma2",
                        @"Soma3",
                        @"Soma4",
                        @"Touristique1",
                        @"Touristique2",
                        @"Touristique3",
                        @"Touristique4",
                        @"Serena1",
                        @"Serena2",
                        @"Serena3",
                        @"Serena4",
                        @"Schweizerhof1",
                        @"Schweizerhof2",
                        @"Schweizerhof3",
                        @"Schweizerhof4",
                        ];

    
    self.clubOverviewImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@1.jpg",self.clubImageNameString]];
    
    self.clubDetailImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@2.jpg",self.clubImageNameString]];
    
    self.clubEventImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@3.jpg",self.clubImageNameString]];
    
    self.clubEssenImage.image  = [UIImage imageNamed:[NSString stringWithFormat:@"%@4.jpg",self.clubImageNameString]];

    
    
    self.clubName.text = self.clubNameString;
    
    self.mainScrollView.contentSize = CGSizeMake(1024, 2304);
    
    
    /*
     Swipable view initialization and self delegation
     */
    [self.swipeableView setNeedsLayout];
    [self.swipeableView layoutIfNeeded];
    
    // required data source
    self.swipeableView.dataSource = self;
    
    // optional delegate
    self.swipeableView.delegate = self;


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)scrollPage:(id)sender{
    UIButton *buttonPressed = sender;
   
    [self changeButtonState:buttonPressed];
    
    // handling the scrolling of the scroll view
    int yCordinateToScroll = (buttonPressed.tag-1)*768;
    [self.mainScrollView setContentOffset:CGPointMake(0, yCordinateToScroll) animated:YES];
    
    
    
}


-(void)changeButtonState:(UIButton *)buttonPressed{
    // three buttons, overview detail and essen;;; change them accordingly
    UIColor *blackTransparentColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.5f];
    UIColor *whiteTransparentColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:0.5f];
    if (buttonPressed.tag == 1) {
        self.overViewButton.backgroundColor = blackTransparentColor;
        [self.overViewButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.detailButton.backgroundColor = whiteTransparentColor;
        [self.detailButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.eventsButton.backgroundColor = whiteTransparentColor;
        [self.eventsButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.essenButton.backgroundColor = whiteTransparentColor;
        [self.essenButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
    }
    else if (buttonPressed.tag == 2){
        self.overViewButton.backgroundColor = whiteTransparentColor;
        [self.overViewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.detailButton.backgroundColor = blackTransparentColor;
        [self.detailButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.eventsButton.backgroundColor = whiteTransparentColor;
        [self.eventsButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.essenButton.backgroundColor = whiteTransparentColor;
        [self.essenButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
    }
    else if(buttonPressed.tag == 3){
        self.overViewButton.backgroundColor = whiteTransparentColor;
        [self.overViewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.detailButton.backgroundColor = whiteTransparentColor;
        [self.detailButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.eventsButton.backgroundColor = blackTransparentColor;
        [self.eventsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.essenButton.backgroundColor = whiteTransparentColor;
        [self.essenButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    else{
        self.overViewButton.backgroundColor = whiteTransparentColor;
        [self.overViewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.detailButton.backgroundColor = whiteTransparentColor;
        [self.detailButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.eventsButton.backgroundColor = whiteTransparentColor;
        [self.eventsButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.essenButton.backgroundColor = blackTransparentColor;
        [self.essenButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




#pragma mark - ZLSwipeableViewDelegate
- (void)swipeableView: (ZLSwipeableView *)swipeableView didSwipeLeft:(UIView *)view {
    //    NSLog(@"did swipe left");
}
- (void)swipeableView: (ZLSwipeableView *)swipeableView didSwipeRight:(UIView *)view {
    //    NSLog(@"did swipe right");
}
- (void)swipeableView:(ZLSwipeableView *)swipeableView didStartSwipingView:(UIView *)view atLocation:(CGPoint)location {
    //    NSLog(@"did start swiping at location: x %f, y%f", location.x, location.y);
}
- (void)swipeableView: (ZLSwipeableView *)swipeableView swipingView:(UIView *)view atLocation:(CGPoint)location {
    //    NSLog(@"swiping at location: x %f, y%f", location.x, location.y);
}
- (void)swipeableView:(ZLSwipeableView *)swipeableView didEndSwipingView:(UIView *)view atLocation:(CGPoint)location {
    //    NSLog(@"did start swiping at location: x %f, y%f", location.x, location.y);
}

#pragma mark - ZLSwipeableViewDataSource
- (UIView *)nextViewForSwipeableView:(ZLSwipeableView *)swipeableView {
    if (self.colorIndex<self.club.clubInfo.gallery.images.count) {
        ClubGallery *view = [[ClubGallery alloc] initWithFrame:swipeableView.bounds];
        view.clubImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, swipeableView.bounds.size.width, swipeableView.bounds.size.height)];
        
        /*
         all the crazy logic goes here
         */
        Image *sampleImage = [self.club.clubInfo.gallery.images objectAtIndex:self.colorIndex];
        NSLog(@"IMAGES URL: %@",sampleImage.originalName);

        [view.clubImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",imgPath,sampleImage.originalName]] placeholderImage:[UIImage imageNamed:@"Agadir1.jpg"]];
        
        view.cardColor = [UIColor colorWithRed:0.61176 green:0 blue:0.0470 alpha:0.5];
        //        view.cardColor = [self colorForName:@"Pomegranate"];
        self.colorIndex++;
        return view;
    }
    else if(self.colorIndex == self.clubImages.count){

//        self.colorIndex = 0;
//        [self.swipeableView discardAllSwipeableViews];
//        [self.swipeableView loadNextSwipeableViewsIfNeeded];
        
    }
    return nil;
}

-(void)loadGalleryImages
{
    self.colorIndex = 0;
    [self.swipeableView discardAllSwipeableViews];
    [self.swipeableView loadNextSwipeableViewsIfNeeded];
    
}



- (IBAction)enableHolidayCheckWidget:(id)sender {
    if ([widgetHolidayCheck isHidden]) {
        [widgetHolidayCheck setHidden:NO];
    }
    else
        [widgetHolidayCheck setHidden:YES];
    
}



- (IBAction)shareEventInformation:(id)sender {
    // Check if the Facebook app is installed and we can present
    // the message dialog
    // Check if the Facebook app is installed and we can present the share dialog
    FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
    params.link = [NSURL URLWithString:sharebleClubDetailUrl];
    
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        // Present the share dialog
        NSLog(@"native app present");
        
        // Present share dialog
        [FBDialogs presentShareDialogWithLink:params.link
                                      handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                          if(error) {
                                              // An error occurred, we need to handle the error
                                              // See: https://developers.facebook.com/docs/ios/errors
                                              NSLog(@"Error publishing story: %@", error.description);
                                          } else {
                                              // Success
                                              NSLog(@"result %@", results);
                                          }
                                      }];
        
    } else {
        // Present the feed dialog
        NSLog(@"use fallback plan");
    }
    
}







- (NSInteger)numberOfSections{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return eventsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cellIdentifier = @"ClubEventCell";
    ClubEventCell  *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.eventLongText.opaque = NO;
    cell.eventLongText.backgroundColor = [UIColor clearColor];
    
    if (cell == nil) {
        
        NSArray *xibViews = [[NSBundle mainBundle] loadNibNamed:@"ClubEventCell" owner:nil options:nil];
        
        Event *event = [[Event alloc] init];
        event = [eventsArray objectAtIndex:indexPath.row];
        NSLog(@"COUNTRY NAME: %@",event.clubShort.country);
        NSLog(@"CLUB NAME: %@",event.clubShort.name_club);
        NSLog(@"EVENT ID: %@",event.id_event);
        cell = (ClubEventCell*)[xibViews lastObject];
        if (event.fact.additional_charge) {
            cell.eventPrice.text = [NSString stringWithFormat:@"%@ €",event.fact.additional_charge];
            [cell.priceView setHidden:NO];
        }
//        for (int i=0; i<eventsIdArray.count; i++) {
//            NSString *storedStringEventId = [NSString stringWithFormat:@"%@",[eventsIdArray objectAtIndex:i]];
//            NSString *realStringEventId = [NSString stringWithFormat:@"%@",event.id_event];
//            if ([realStringEventId isEqualToString:storedStringEventId]) {
//                cell.shareUrlString = [eventsUrlPageArray objectAtIndex:i];
//                NSLog(@"EVENT ID: %@",[eventsIdArray objectAtIndex:i]);
//                NSLog(@"EVENT URL: %@",[eventsUrlPageArray objectAtIndex:i]);
//                break;
//            }
//        }
        //        if([eventsIdArray containsObject:event.id_event])
        //        {
        //
        //        }
        [cell.eventLongText loadHTMLString:event.long_text baseURL:nil];
        cell.eventName.text = event.name;
//        cell.clubImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"event_%@.jpg",event.clubShort.id_club]];
        cell.flag_image.image = [UIImage imageNamed:[NSString stringWithFormat:@"flag_%@.png",event.clubShort.id_club]];
        [cell.eventImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg",mediaImagePath,event.id_event]]];
        cell.countryName = event.clubShort.country;
        cell.backgroundColor = [UIColor clearColor];
    }
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 768;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

}

//-(void)showWikiPopup:(NSString *)errorMessage{
-(void)showWikiPopup:(NSString *)country{
    WikiPopUp *statuspopup = [WikiPopUp loadWithNib];
    [statuspopup.closeButton addTarget:self action:@selector(dismissPopupView) forControlEvents:UIControlEventTouchUpInside];
    [statuspopup setTag:100];
    [statuspopup show];
}
- (void)dismissPopupView{
    
    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    
    if (!window) {
        window = ([UIApplication sharedApplication].windows)[0];
    }
    DialogView *currentPopup = (DialogView *)[window viewWithTag:100];
    [currentPopup cancel];
}
@end
