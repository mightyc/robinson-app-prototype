//
//  ClubGallery.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 19/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <UIKit/UIKit.h>

@interface ClubGallery : UIView

@property UIColor *cardColor;
@property (nonatomic, retain) UIImageView *clubImage;
@end
