//
//  ClubShort.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 28/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "ClubShort.h"

@implementation ClubShort
@synthesize id_club = mId_club;
@synthesize name_club = mName_club;
@synthesize code = mCode;
@synthesize product_code = mProduct_code;
@synthesize street = mStreet;
@synthesize zipCode = mZipCode;
@synthesize city = mCity;
@synthesize region = mRegion;
@synthesize country = mCountry;
@synthesize latitude = mLatitude;
@synthesize longitude = mLongitude;
@synthesize phone = mPhone;
@synthesize fax = mFax;
@synthesize email = mEmail;
@synthesize giata = mGiata;
@synthesize name_in_xml = mName_in_xml;
- (id)initWithDictionary:(NSDictionary *)_data{
    
    if (_data == nil) { return nil;}
    
    self = [super init];
    
    if (self) {        
        mId_club = [_data[@"id"] copy];
        mName_club = [_data[@"name"] copy];
        mCode = [_data[@"code"] copy];
        mProduct_code = [_data[@"product_code"] copy];
        mStreet = [_data[@"street"] copy];
        mZipCode = [_data[@"zipcode"] copy];
        mCity = [_data[@"city"] copy];
        mRegion = [_data[@"region"] copy];
        mCountry = [_data[@"country"] copy];
        mLongitude = [_data[@"longitude"] copy];
        mLatitude = [_data[@"latitude"] copy];
        mPhone = [_data[@"phone"] copy];
        mFax = [_data[@"fax"] copy];
        mEmail = [_data[@"email"] copy];
        mGiata = [_data[@"giata"] copy];
        mName_in_xml = [_data[@"name_in_xml"] copy];
    }
    return self;
}

@end
