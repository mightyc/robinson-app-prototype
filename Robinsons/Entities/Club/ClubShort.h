//
//  ClubShort.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 28/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClubShort : NSObject
{ 
    NSString *mId_club;
    NSString *mName_club;
    NSString *mCode;
    NSString *mProduct_code;
    NSString *mStreet;
    NSString *mZipCode;
    NSString *mCity;
    NSString *mRegion;
    NSString *mCountry;
    NSString *mLatitude;
    NSString *mLongitude;
    NSString *mPhone;
    NSString *mFax;
    NSString *mEmail;
    NSString *mGiata;
    NSString *mName_in_xml;
}
@property (nonatomic, retain) NSString *id_club;
@property (nonatomic, retain) NSString *name_club;
@property (nonatomic, retain) NSString *code;
@property (nonatomic, retain) NSString *product_code;
@property (nonatomic, retain) NSString *street;
@property (nonatomic, retain) NSString *zipCode;
@property (nonatomic, retain) NSString *city;
@property (nonatomic, retain) NSString *region;
@property (nonatomic, retain) NSString *country;
@property (nonatomic, retain) NSString *latitude;
@property (nonatomic, retain) NSString *longitude;
@property (nonatomic, retain) NSString *phone;
@property (nonatomic, retain) NSString *fax;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *giata;
@property (nonatomic, retain) NSString *name_in_xml;

- (id)initWithDictionary:(NSDictionary *)_data;
@end
