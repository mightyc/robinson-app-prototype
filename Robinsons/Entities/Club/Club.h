//
//  Club.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 19/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Image.h"
#import "ClubInfo.h"
@interface Club : NSObject
{
    ClubInfo *mClubInfo;
    NSString *mSeason_club_detail_url;
}
@property (nonatomic, retain) ClubInfo *clubInfo;
@property (nonatomic, retain) NSString *season_club_detail_url;

- (id)initWithDictionary:(NSDictionary *)_data;
@end
