//
//  Club.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 19/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "Club.h"

@implementation Club
@synthesize clubInfo = mClubInfo;
@synthesize season_club_detail_url = mSeason_club_detail_url;
- (id)initWithDictionary:(NSDictionary *)_data{
    
    if (_data == nil) { return nil;}
    
    self = [super init];
    
    if (self) {
        mSeason_club_detail_url = _data[@"season_club_detail_url"];        
        mClubInfo = [[ClubInfo alloc] initWithDictionary:_data[@"club_infos"]];
        NSLog(@"Club Info: %@",mClubInfo);
    }
    return self;
}

@end
