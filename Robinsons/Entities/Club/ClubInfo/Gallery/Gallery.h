//
//  Gallery.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 20/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Image.h"
@interface Gallery : NSObject
{
    NSString *mObject_id;
    NSString *mName;
    
    
    NSMutableArray *mImages;
    Image *mImage;
}

@property (nonatomic, retain) NSString *object_id;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSMutableArray *images;
@property (nonatomic, retain) Image *image;
- (id)initWithDictionary:(NSDictionary *)_data;
@end
