//
//  Gallery.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 20/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "Gallery.h"

@implementation Gallery
@synthesize object_id = mObject_id;
@synthesize name = mName;
@synthesize images = mImages;
@synthesize image = mImage;
- (id)initWithDictionary:(NSDictionary *)_data{
    
    if (_data == nil) { return nil;}
    
    self = [super init];
    
    if (self) {
        
        mObject_id = [_data[@"intro_long"] copy];
        mName = [_data[@"intro_description"] copy];
        mImages = [[NSMutableArray alloc] init];
        
        NSMutableArray *temporaryImagesArray = [[NSMutableArray alloc] init];
        temporaryImagesArray = [_data[@"images"] copy];
        
        for (int i = 0; i<temporaryImagesArray.count; i++) {
            mImage = [[Image alloc] initWithDictionary:[temporaryImagesArray objectAtIndex:i]];
            [mImages insertObject:mImage atIndex:i];

        }
    }
    return self;
}


@end
