//
//  EatAndDrink.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 20/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "EatAndDrink.h"

@implementation EatAndDrink
@synthesize left_column = mLeft_column;
@synthesize right_column = mRight_column;
@synthesize link_url = mLink_url;
@synthesize link_text = mLink_text;


- (id)initWithDictionary:(NSDictionary *)_data{
    
    if (_data == nil) { return nil;}
    
    self = [super init];
    
    if (self) {
        mLeft_column = [_data[@"left_column"] copy];
        mRight_column = [_data[@"right_column"] copy];
        mLink_url = [_data[@"right_column"] copy];
        mLink_text = [_data[@"link_text"] copy];
    }
    return self;
}

@end
