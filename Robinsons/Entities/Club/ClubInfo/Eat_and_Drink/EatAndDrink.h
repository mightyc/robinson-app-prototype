//
//  EatAndDrink.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 20/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EatAndDrink : NSObject
{
    NSString *mLeft_column;
    NSString *mRight_column;
    NSString *mLink_url;
    NSString *mLink_text;
}
@property (nonatomic, retain) NSString *left_column;
@property (nonatomic, retain) NSString *right_column;
@property (nonatomic, retain) NSString *link_url;
@property (nonatomic, retain) NSString *link_text;
- (id)initWithDictionary:(NSDictionary *)_data;
@end
