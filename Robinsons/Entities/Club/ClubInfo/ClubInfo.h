//
//  ClubInfo.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 20/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Gallery.h"
#import "EatAndDrink.h"
@interface ClubInfo : NSObject{
    NSString *mIntro_long;
    NSString *mIntro_description;
    NSString *mClub_type;
    NSString *mArrival_day;
    NSString *mArrival_day_main;
    NSString *mArrival_long;
    NSString *mTrain;
    NSString *mTransfer;
    
    
    NSString *mIcon_holidayCheck;
    NSString *mWidget_holidayCheck;
    
    
    NSString *mVideo_id;
    NSString *mPanorama_url;
    
    Gallery *mGallery;
    EatAndDrink *mEatAndDrink;
    
    
    

}

@property(nonatomic, retain) NSString *intro_long;
@property(nonatomic, retain) NSString *intro_description;
@property(nonatomic, retain) NSString *club_type;
@property(nonatomic, retain) NSString *arrival_day;
@property(nonatomic, retain) NSString *arrival_day_main;
@property(nonatomic, retain) NSString *arrival_long;
@property(nonatomic, retain) NSString *train;
@property(nonatomic, retain) NSString *transfer;

@property (nonatomic, retain) NSString *icon_holidayCheck;
@property (nonatomic, retain) NSString *widget_holidayCheck;

@property(nonatomic, retain) NSString *video_id;
@property(nonatomic, retain) NSString *panorama_url;
@property(nonatomic, retain) Gallery *gallery;
@property(nonatomic, retain) EatAndDrink *eatAndDrink;

- (id)initWithDictionary:(NSDictionary *)_data;

@end
