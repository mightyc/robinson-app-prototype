//
//  ClubInfo.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 20/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "ClubInfo.h"

@implementation ClubInfo
@synthesize intro_long = mIntro_long;
@synthesize intro_description = mIntro_description;
@synthesize club_type = mClub_type;
@synthesize arrival_day = mArrival_day;
@synthesize arrival_day_main = mArrival_day_main;
@synthesize arrival_long = mArrival_long;
@synthesize train = mTrain;
@synthesize transfer = mTransfer;

@synthesize icon_holidayCheck = mIcon_holidayCheck;
@synthesize widget_holidayCheck = mWidget_holidayCheck;

@synthesize video_id = mVideo_id;
@synthesize panorama_url = mPanorama_url;
@synthesize gallery = mGallery;
@synthesize eatAndDrink = mEatAndDrink;

- (id)initWithDictionary:(NSDictionary *)_data{
    
    if (_data == nil) { return nil;}
    
    self = [super init];
    
    if (self) {
        
        mIntro_long = [_data[@"intro_long"] copy];
        mIntro_description = [_data[@"intro_description"] copy];
        mClub_type = [_data[@"club_type"] copy];
        mArrival_day = [_data[@"arrival_day"] copy];
        mArrival_day_main = [_data[@"arrival_day_main"] copy];
        
        mArrival_long = [_data[@"arrival_long"] copy];
        
        mTrain = [_data[@"train"] copy];
        mTransfer = [_data[@"transfer"] copy];
        
        mIcon_holidayCheck = [_data[@"icon_holidaycheck"] copy];
        mWidget_holidayCheck = [_data[@"widget_holidaycheck_m"] copy];
        
        mVideo_id = [_data[@"video_id"] copy];
        mPanorama_url = [_data[@"panorama_url"] copy];
        mGallery = [[Gallery alloc] initWithDictionary:_data[@"gallery"]];
        mEatAndDrink = [[EatAndDrink alloc] initWithDictionary:_data[@"eat_and_drink"]];
        
        
        
    }
    return self;
}

@end
