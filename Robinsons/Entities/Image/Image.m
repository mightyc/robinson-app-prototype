//
//  Image.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 20/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "Image.h"

@implementation Image

@synthesize descriptionImage = mDescriptionImage;
@synthesize imageCategory = mImageCategory;
@synthesize name = mName;
@synthesize objectId = mObjectId;
@synthesize originalName = mOriginalName;
@synthesize title = mTitle;


- (id)initWithDictionary:(NSDictionary *)_data{
    
    if (_data == nil) { return nil;}
    
    self = [super init];
    
    if (self) {
        
        mDescriptionImage = [_data[@"description"] copy];
        mImageCategory = [_data[@"image_category"] copy];
        mName = [_data[@"name"] copy];
        mObjectId = [_data[@"object_id"] copy];
        mOriginalName = [_data[@"original_name"] copy];
        mTitle = [_data[@"title"] copy];
    }
    return self;
}

@end
