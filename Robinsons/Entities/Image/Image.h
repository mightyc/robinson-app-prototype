//
//  Image.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 20/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Image : NSObject
{
    NSString *mDescriptionImage;
    NSString *mImageCategory;
    NSString *mName;
    NSString *mObjectId;
    NSString *mOriginalName;
    NSString *mTitle;
}

@property (nonatomic, retain) NSString *descriptionImage;
@property (nonatomic, retain) NSString *imageCategory;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *objectId;
@property (nonatomic, retain) NSString *originalName;
@property (nonatomic, retain) NSString *title;

- (id)initWithDictionary:(NSDictionary *)_data;
@end
