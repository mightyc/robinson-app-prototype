//
//  Fact.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 23/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "Fact.h"

@implementation Fact

@synthesize additional_charge = mAdditional_charge;
@synthesize booking_text = mBooking_text;
@synthesize event_id = mEvent_id;
@synthesize power_level_name = mPower_level_name;
@synthesize long_text = mLong_text;
- (id)initWithDictionary:(NSDictionary *)_data{
    
    if (_data == nil) { return nil;}
    
    self = [super init];
    
    if (self) {
        
        mAdditional_charge = [_data[@"additional_charge"] copy];
        mBooking_text = [_data[@"booking_text"] copy];
        mEvent_id = [_data[@"event_id"] copy];
        
        NSMutableDictionary *power_level = [[NSMutableDictionary alloc] init];
        power_level = [_data[@"power_level"] copy];
        
        mPower_level_name = [power_level objectForKey:@"name"];
        
        mLong_text = [_data[@"text"] copy];        
    }
    return self;
}

@end
