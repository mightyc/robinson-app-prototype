//
//  Fact.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 23/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fact : NSObject
{

    NSString *mAdditional_charge;
    NSString *mBooking_text;
    NSString *mEvent_id;
    NSString *mPower_level_name;
    
    NSString *mLong_text;
    
    
}
@property (nonatomic, retain) NSString *additional_charge;
@property (nonatomic, retain) NSString *booking_text;
@property (nonatomic, retain) NSString *event_id;
@property (nonatomic, retain) NSString *power_level_name;
@property (nonatomic, retain) NSString *long_text;
- (id)initWithDictionary:(NSDictionary *)_data;

@end
