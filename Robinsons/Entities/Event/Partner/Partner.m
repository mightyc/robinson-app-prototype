//
//  Partnet.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 23/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "Partner.h"

@implementation Partner
@synthesize name = mName;
@synthesize description = mDescription;
@synthesize link = mLink;
@synthesize imagesArray = mImagesArray;
@synthesize image = mImage;
- (id)initWithDictionary:(NSDictionary *)_data{
    
    if (_data == nil) { return nil;}
    
    self = [super init];
    
    if (self) {
        mName = [_data[@"name"] copy];
        mDescription = [_data[@"description"] copy];
        
        mLink = [[Link alloc] initWithDictionary:_data[@"link"]];
        
        mImagesArray = [[NSMutableArray alloc] init];
        
        NSMutableArray *temporaryImagesArray = [[NSMutableArray alloc] init];
        temporaryImagesArray = [_data[@"images"] copy];
        
        for (int i = 0; i<temporaryImagesArray.count; i++) {
            mImage = [[Image alloc] initWithDictionary:[temporaryImagesArray objectAtIndex:i]];
            [mImagesArray insertObject:mImage atIndex:i];            
        }

    }
    return self;
}

@end
