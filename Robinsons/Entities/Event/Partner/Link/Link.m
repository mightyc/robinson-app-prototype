//
//  Link.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 23/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "Link.h"

@implementation Link
@synthesize title = mTitle;
@synthesize url = mUrl;
@synthesize type = mType;
- (id)initWithDictionary:(NSDictionary *)_data{
    
    if (_data == nil) { return nil;}
    
    self = [super init];
    
    if (self) {
        mTitle = [_data[@"title"] copy];
        mUrl = [_data[@"url"] copy];
        mType = [_data[@"type"] copy];
    }
    return self;
}

@end
