//
//  Link.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 23/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Link : NSObject
{
    NSString *mTitle;
    NSString *mUrl;
    NSString *mType;    
}
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSString *type;
- (id)initWithDictionary:(NSDictionary *)_data;
@end
