//
//  Partnet.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 23/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Link.h"
#import "Image.h"
@interface Partner : NSObject
{
    NSString *mName;
    NSString *mDescription;
    
    Link *mLink;
    NSMutableArray *mImagesArray;
    
    Image *mImage;
}
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) Link *link;
@property (nonatomic, retain) NSMutableArray *imagesArray;
@property (nonatomic, retain) Image *image;
- (id)initWithDictionary:(NSDictionary *)_data;
@end
