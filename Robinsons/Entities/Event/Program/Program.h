//
//  Program.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 23/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Program : NSObject
{
    NSString *mWeekday;
    NSString *mName;
    NSString *mSort;
}
@property (nonatomic, retain) NSString *weekday;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *sort;
- (id)initWithDictionary:(NSDictionary *)_data;
@end
