//
//  Program.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 23/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "Program.h"

@implementation Program
@synthesize weekday = mWeekday;
@synthesize name = mName;
@synthesize sort = mSort;
- (id)initWithDictionary:(NSDictionary *)_data{
    
    if (_data == nil) { return nil;}
    
    self = [super init];
    
    if (self) {        
        mWeekday = [_data[@"weekday"] copy];
        mName = [_data[@"name"] copy];
        mSort = [_data[@"sort"] copy];
    }
    return self;
}

@end
