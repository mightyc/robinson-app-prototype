//
//  Event.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 23/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Fact.h"
#import "Program.h"
#import "Partner.h"
#import "Season.h"
#import "ClubShort.h"
@interface Event : NSObject
{
    ClubShort *mClubShort;
    
    NSString *mObject_id;
    NSString *mId_event;
    NSString *mName;
    
    NSString *mCategoryId;
    NSString *mCategoryName;

    
    NSString *mStart_date;
    NSString *mEnd_date;
    NSString *mShort_text;
    NSString *mLong_text;
    
    NSString *mTheme;
    
    Fact *mFact;
    Program *mProgram;
    Season *mSeason;
    Partner *mPartner;

    
    NSMutableArray *mPartnersArray;
    
    NSMutableArray *mProgramsArray;
    
}
@property (nonatomic, retain) ClubShort *clubShort;
@property (nonatomic, retain) NSString *object_id;
@property (nonatomic, retain) NSString *id_event;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *categoryId;
@property (nonatomic, retain) NSString *categoryName;
@property (nonatomic, retain) NSString *start_date;
@property (nonatomic, retain) NSString *end_date;
@property (nonatomic, retain) NSString *short_text;
@property (nonatomic, retain) NSString *long_text;
@property (nonatomic, retain) NSString *theme;

@property (nonatomic, retain) Fact *fact;
@property (nonatomic, retain) Program *program;
@property (nonatomic, retain) Season *season;
@property (nonatomic, retain) Partner *partner;

@property (nonatomic, retain) NSMutableArray *partnersArray;
@property (nonatomic, retain) NSMutableArray *programsArray;

- (id)initWithDictionary:(NSDictionary *)_data;

@end
