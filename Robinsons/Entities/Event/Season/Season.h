//
//  Season.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 26/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Season : NSObject
{
    NSString *mIdSeason;
    NSString *mNameSeason;
    NSString *mType;
    NSString *mStartDate;
    NSString *mEndDate;
}
@property (nonatomic, retain) NSString *idSeason;
@property (nonatomic, retain) NSString *nameSeason;
@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSString *start_date;
@property (nonatomic, retain) NSString *end_date;
- (id)initWithDictionary:(NSDictionary *)_data;
@end
