//
//  Season.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 26/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "Season.h"

@implementation Season
@synthesize idSeason = mIdSeason;
@synthesize nameSeason = mNameSeason;
@synthesize type = mType;
@synthesize start_date = mStartDate;
@synthesize end_date = mEndDate;

- (id)initWithDictionary:(NSDictionary *)_data{
    
    if (_data == nil) { return nil;}
    
    self = [super init];
    
    if (self) {

        mIdSeason = [_data[@"id"] copy];
        mNameSeason = [_data[@"name"] copy];
        mType = [_data[@"type"] copy];
        mStartDate = [_data[@"start_date"] copy];
        mEndDate = [_data[@"end_date"] copy];
    }
    return self;
}

@end
