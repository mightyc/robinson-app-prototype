//
//  Event.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 23/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "Event.h"

@implementation Event
@synthesize clubShort = mClubShort;
@synthesize object_id = mObject_id;
@synthesize id_event = mId_event;
@synthesize name = mName;
@synthesize categoryId = mCategoryId;
@synthesize categoryName = mCategoryName;
@synthesize start_date = mStart_date;
@synthesize end_date = mEnd_date;
@synthesize short_text = mShort_text;
@synthesize long_text = mLong_text;
@synthesize theme = mTheme;
@synthesize fact = mFact;
@synthesize program = mProgram;
@synthesize season = mSeason;
@synthesize partner = mPartner;
@synthesize partnersArray = mPartnersArray;
@synthesize programsArray = mProgramsArray;
- (id)initWithDictionary:(NSDictionary *)_data{
    
    if (_data == nil) { return nil;}
    
    self = [super init];
    
    if (self) {
        mObject_id = [_data[@"object_id"] copy];
        mId_event = [_data[@"id"] copy];
        mName = [_data[@"name"] copy];
        
        mClubShort = [[ClubShort alloc] initWithDictionary:_data[@"club"]];
        
        NSMutableDictionary *category = [[NSMutableDictionary alloc] init];
        category = [_data[@"category"] copy];
        
        mCategoryId = [category objectForKey:@"id"];
        mCategoryName = [category objectForKey:@"name"];

        mStart_date = [_data[@"start_date"] copy];
        mEnd_date = [_data[@"end_date"] copy];
        mShort_text = [_data[@"short_text"] copy];
        mLong_text = [_data[@"long_text"] copy];
        mTheme = [_data[@"theme"] copy];
        
        mFact = [[Fact alloc] initWithDictionary:_data[@"facts"]];
        
        
        
        mProgramsArray = [[NSMutableArray alloc] init];
        
        NSMutableArray *temporaryProgramsArray = [[NSMutableArray alloc] init];
        temporaryProgramsArray = [_data[@"programs"] copy];
        
        for (int i = 0; i<temporaryProgramsArray.count; i++) {
            mProgram = [[Program alloc] initWithDictionary:[temporaryProgramsArray objectAtIndex:i]];
            [mProgramsArray insertObject:mProgram atIndex:i];
            
        }
        
        mSeason = [[Season alloc] initWithDictionary:_data[@"season"]];
        
        mPartnersArray = [[NSMutableArray alloc] init];
        
        NSMutableArray *temporaryPartnersArray = [[NSMutableArray alloc] init];
        temporaryPartnersArray = [_data[@"partners"] copy];
        
        for (int i = 0; i<temporaryPartnersArray.count; i++) {
            mPartner = [[Partner alloc] initWithDictionary:[temporaryPartnersArray objectAtIndex:i]];
            [mPartnersArray insertObject:mPartner atIndex:i];
            
        }
        

    }
    return self;
}

@end
