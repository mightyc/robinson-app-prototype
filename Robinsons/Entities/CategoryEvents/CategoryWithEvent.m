//
//  CategoryWithEvent.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 29/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "CategoryWithEvent.h"

@implementation CategoryWithEvent
@synthesize categoryName = mCategoryName;
@synthesize categoryId = mCategoryId;
@synthesize categoryImageName = mCategoryImageName;

@end

