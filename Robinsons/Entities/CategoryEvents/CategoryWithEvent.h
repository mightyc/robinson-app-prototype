//
//  CategoryWithEvent.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 29/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryWithEvent : NSObject
{
    NSString *mCategoryName;
    NSString *mCategoryId;
    NSString *mCategoryImageName;
}
@property(nonatomic, retain) NSString *categoryName;
@property(nonatomic, retain) NSString *categoryId;
@property(nonatomic, retain) NSString *categoryImageName;

@end

