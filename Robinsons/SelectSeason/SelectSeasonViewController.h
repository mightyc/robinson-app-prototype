//
//  SelectSeasonViewController.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 12/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBLoginView.h>

@interface SelectSeasonViewController : UIViewController <FBLoginViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *summerButton;
@property (weak, nonatomic) IBOutlet UIButton *winterButton;
//@property (weak, nonatomic) IBOutlet FBLoginView *loginView;

@property (nonatomic, retain) NSString *startDate;
@property (nonatomic, retain) NSString *endDate;

@property (nonatomic, retain) NSString *seasonId;

@property (nonatomic, retain) NSString *facebookUserId;
@property (nonatomic, retain) NSArray *fbUserFriendlist;

@end
