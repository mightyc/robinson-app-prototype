//
//  SelectSeasonViewController.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 12/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "SelectSeasonViewController.h"
#import "SelectTypeViewController.h"
#import <FacebookSDK/FacebookSDK.h>

@interface SelectSeasonViewController ()

@end

@implementation SelectSeasonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    FBLoginView *loginView = [[FBLoginView alloc] initWithReadPermissions:@[@"public_profile", @"email", @"user_friends"]];
    loginView.center = self.view.center;
    loginView.delegate = self;
    [self.view addSubview:loginView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
}
//Save fbUserId of logged in use for further use
- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    self.facebookUserId = user.objectID;
    [FBRequestConnection startWithGraphPath:@"me/taggable_friends" parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        self.fbUserFriendlist = result;
        NSMutableDictionary *friendsDictionary = [[NSMutableDictionary alloc] init];
        friendsDictionary = result;
        NSMutableArray *friendsDataArray = [[NSMutableArray alloc] init];
        friendsDataArray = [friendsDictionary objectForKey:@"data"];
        NSLog(@"facebook friendlist count: %d",friendsDataArray.count);
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)seasonSelection:(id)sender{
    UIButton *button = sender;
//    self.navigationController.navigationBarHidden = NO;
    SelectTypeViewController* viewController = [[SelectTypeViewController alloc] init];
    if(button.tag == 1){
        viewController.seasonType = @"winter";
        viewController.seasonId = @"9";
        self.startDate = @"2015-02-04";
        self.endDate = @"2015-04-30";
        viewController.startDate = self.startDate;
        viewController.endDate = self.endDate;
        //winter 14/15
    }
    else{
        viewController.seasonType = @"summer";
        viewController.seasonId = @"10";
        self.startDate = @"2015-05-01";
        self.endDate = @"2015-10-31";
        viewController.startDate = self.startDate;
        viewController.endDate = self.endDate;        
        //summer 15
    }
    [self.navigationController pushViewController:viewController animated:YES];

}
@end
