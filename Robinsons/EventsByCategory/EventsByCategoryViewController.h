//
//  EventsByCategoryViewController.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 27/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsByCategoryViewController : UIViewController
{
    NSString *eventsUrl;
    NSString *imgPath;
    
    NSMutableArray *eventsArray;
    
    NSMutableArray *eventsIdArray;
    NSMutableArray *eventsUrlPageArray;
}
@property (weak, nonatomic) IBOutlet UITableView *eventsTable;
@property (nonatomic, retain) NSString *startDate;
@property (nonatomic, retain) NSString *endDate;

@property (nonatomic, retain) NSString *categoryIdsList;

@end
