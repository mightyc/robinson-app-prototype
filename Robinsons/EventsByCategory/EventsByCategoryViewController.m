//
//  EventsByCategoryViewController.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 27/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "EventsByCategoryViewController.h"
#import "EventCell.h"
#import <AFNetworking/AFNetworking.h>
#import "Event.h"
#import "Fact.h"
#import "UIImageView+AFNetworking.h"
#import "WikiPopUp.h"

@interface EventsByCategoryViewController ()

@end

@implementation EventsByCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    eventsIdArray = [[NSMutableArray alloc] init];
    eventsUrlPageArray = [[NSMutableArray alloc] init];
    
    [eventsIdArray addObject:@"3351"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/outdoor/topsommer/kletter-woche"];
    
    [eventsIdArray addObject:@"3360"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/am-ball/winter/beach-volleyball-camp"];
    
    [eventsIdArray addObject:@"3349"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/outdoor-so15/topsommer/mountainbike-woche-so-15"];
    
    [eventsIdArray addObject:@"3355"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/sport-intensiv/top-events-sommer-2015/box-camp-cala-serena-1005-170515-so15"];
    
    [eventsIdArray addObject:@"3356"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/sport-intensiv/top-events-sommer-2015/box-camp-camyuva-1809-250915-so-15"];
    
    [eventsIdArray addObject:@"3382"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/lebensart/top-events-sommer/gop-variete-woche-landskron-so-2015"];
    
    [eventsIdArray addObject:@"3368"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/golf/top-event-sommer-2015/nobilis-spring-golf-week-2015-so-15"];
    
    [eventsIdArray addObject:@"3244"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/cluburlaub/portugal/quinta-da-ria/winter/club-events?page=3&categoryIds=&productCodes=FAO26026&partnerIds=&topEvent=0&startDate=2015-02-04&endDate="];
    
    [eventsIdArray addObject:@"3374"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/kinder-und-jugendliche/sommer-2015/benjamin-bluemchen-so-15"];
    
    [eventsIdArray addObject:@"3372"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/kinder-und-jugendliche/sommer-2015/der-ganze-club-tanzt-kalimera-kriti-so15"];
    
    [eventsIdArray addObject:@"3480"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/kinder-und-jugendliche/sommer-2015/der-ganze-club-tanzt-nobilis-sommer-2015"];
    
    [eventsIdArray addObject:@"3378"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/kinder-und-jugendliche/sommer-2015/soccer-camp-apulia-sommer-2015"];
    
    [eventsIdArray addObject:@"3369"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/kinder-und-jugendliche/sommer-2015/handball-camp-so-15"];
    [eventsIdArray addObject:@"3376"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/kinder-und-jugendliche/sommer-2015/96-fussball-camp-so-15"];
    
    [eventsIdArray addObject:@"3379"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/kinder-und-jugendliche/sommer-2015/soccer-camp-fleesensee-so15"];
    
    [eventsIdArray addObject:@"3380"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/kinder-und-jugendliche/sommer-2015/soccer-camp-fleesensee-so15"];
    [eventsIdArray addObject:@"3373"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/kinder-und-jugendliche/sommer-2015/der-ganze-club-tanzt-daidalos-sommer-2015"];
    
    [eventsIdArray addObject:@"3354"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/outdoor/topsommer/lauf-camp-fleesensee-so-15"];
    
    [eventsIdArray addObject:@"3353"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/outdoor/topsommer/lauf-camp-landskron-so-15"];
    
    [eventsIdArray addObject:@"3340"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/lebensart/top-events-sommer/palladium-meets-k-swiss-so-15"];
    
    [eventsIdArray addObject:@"3338"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/lebensart/top-events-sommer/llambi-tanzt-so15"];
    
    [eventsIdArray addObject:@"3339"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/lebensart/top-events-sommer/gourmet-woche-so15"];
    
    [eventsIdArray addObject:@"3358"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/event-highlights-partner/robinson-adidas-fecht-wochen-so-15"];
    
    [eventsIdArray addObject:@"3357"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/sport-intensiv/top-events-sommer-2015/biathlon-camp-so-15"];
    
    [eventsIdArray addObject:@"3472"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/event-highlights-partner/robinson-adidas-fecht-wochen-so-15"];
    
    [eventsIdArray addObject:@"3381"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/tennis/top-event-sommer-2015/tennis-camp-jandia"];
    
    [eventsIdArray addObject:@"3362"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/am-ball/sommer-2015/tischtennis-camp-so-15"];
    
    [eventsIdArray addObject:@"3352"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/sport-intensiv/top-events-sommer-2015/triathlon-camp-cala-serena-so-15"];
    
    [eventsIdArray addObject:@"3364"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/wassersport/top-event-sommer-2015/surf-week-so-15"];
    
    [eventsIdArray addObject:@"3363"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/wassersport/top-event-sommer-2015/catamaran-week-so-15"];
    
    [eventsIdArray addObject:@"3346"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/wellfit/top-events-sommer/wellfit-camp-camyuva-0805-150515"];
    
    [eventsIdArray addObject:@"3344"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/wellfit/top-events-sommer/east-meets-west-by-evian-so-15"];
    
    [eventsIdArray addObject:@"3345"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/wellfit/top-events-sommer/wasser-ist-bewegung-so-15"];
    
    [eventsIdArray addObject:@"3342"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/wellfit/top-events-sommer/yoga-body-boost-so-15"];
    
    
    [eventsIdArray addObject:@"3347"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/wellfit/top-events-sommer/safs-fitness-party-camp-so15"];
    
    
    //======================SUMMER TOP EVENTS
    
    
    
//startDate=2015-02-04&endDate=2015-04-30
    
    [eventsIdArray addObject:@"3188"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/am-ball/winter/beach-volleyball-camp"];
    
    [eventsIdArray addObject:@"3591"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/cluburlaub/tuerkei/nobilis/winter/club-events"];
    
    [eventsIdArray addObject:@"3384"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/cluburlaub/tuerkei/camyuva/sommer/club-events"];
    
    [eventsIdArray addObject:@"3180"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/outdoor/top-event-winter-14-15/rennrad-woche-winter-14-15"];
    
    [eventsIdArray addObject:@"3181"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/outdoor/top-event-winter-14-15/rennrad-woche-wi14-15"];
    
    [eventsIdArray addObject:@"3195"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/lebensart/top-events-winter-14-15/gop-variete-woche-schweizerhof-wi-14-15"];
    
    [eventsIdArray addObject:@"3178"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/lebensart/top-events-winter-14-15/gop-variete-woche-alpenrose-zuers-wi-14-15"];
    
    [eventsIdArray addObject:@"3193"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/golf/top-event-winter-2014/15/robinson-golf-knock-out-wi14-15"];
    
    [eventsIdArray addObject:@"3194"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/golf/top-event-winter-2014/15/so-geht-golf-wi14-15"];
    
    [eventsIdArray addObject:@"3471"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/kinder-und-jugendliche/sommer-2015/tanz-dich-fit-2903-05042015"];
    
    [eventsIdArray addObject:@"3375"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/kinder-und-jugendliche/sommer-2015/die-wilden-kerle-camp-sommer-2015"];
    
    [eventsIdArray addObject:@"3525"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/cluburlaub/spanien/playa-granada/sommer/club-events"];
    
    [eventsIdArray addObject:@"3337"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/lebensart/top-events-sommer/robinson-goes-classic-2015"];
    
    [eventsIdArray addObject:@"3383"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/cluburlaub/tuerkei/camyuva/sommer/club-events"];
    
    [eventsIdArray addObject:@"3365"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/tennis/top-event-sommer-2015/tennis-camp-camyuva-1804-250415-so-15"];
    
    [eventsIdArray addObject:@"3366"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/tennis/top-event-sommer-2015/tennis-camp-sarigerme-2004-2704/2704-04052015-so-15"];
    
    [eventsIdArray addObject:@"3183"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/outdoor/top-event-winter-14-15/triathlon-camp-wi14-15"];
    
    [eventsIdArray addObject:@"3191"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/wellfit/top-events-winter-14-15/wellfitr-camp-soma-bay"];
    
    [eventsIdArray addObject:@"3192"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/wellfit/top-events-winter-14-15/wellfitr-camp-esquinzo-playa"];
    
    [eventsIdArray addObject:@"3197"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/wellfit/top-events-winter-14-15/bewegter-ruecken-wi-14-15"];
    
    [eventsIdArray addObject:@"3341"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/wellfit/top-events-sommer/ganz-schoen-gesund-sein-sommer15"];
    
    [eventsIdArray addObject:@"3079"];
    [eventsUrlPageArray addObject:@"http://www.robinson.com/de/de/events/dein-urlaub-deine-events/wintersport/top-events-winter-14-15/head-ski-snowboard-testwoche-wi-14-15"];
    
    
    
    ////////////////////
    
    imgPath = @"http://media-index.robinson.plusline.de/clubevents/";
    eventsArray = [[NSMutableArray alloc] init];
    
    NSLog(@"START DATE: %@",self.startDate);
    NSLog(@"END DATE: %@",self.endDate);
    NSLog(@"Category Id: %@",self.categoryIdsList);
    eventsUrl = [NSString stringWithFormat:@"http://app:IToor8ah@robinson.stage.mtc.fcse.io/api/events/de_DE/category_ids/%@?_format=json&startDate=%@&endDate=%@&isTopEvent=true&size=50&from=0",self.categoryIdsList,self.startDate,self.endDate];
    
    
    AFHTTPRequestOperationManager *eventsManager = [AFHTTPRequestOperationManager manager];
    [eventsManager GET:eventsUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSMutableArray *temporaryEventsArray = [[NSMutableArray alloc] init];
        temporaryEventsArray = responseObject;
        for (int i = 0; i<temporaryEventsArray.count; i++) {
            Event *event = [[Event alloc] initWithDictionary:[temporaryEventsArray objectAtIndex:i]];
            [eventsArray addObject:event];
        }
        [self.eventsTable reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSections{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return eventsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cellIdentifier = @"EventCell";
    EventCell  *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.eventLongText.opaque = NO;
    cell.eventLongText.backgroundColor = [UIColor clearColor];
    
    if (cell == nil) {
        
        NSArray *xibViews = [[NSBundle mainBundle] loadNibNamed:@"EventCell" owner:nil options:nil];
        
        Event *event = [[Event alloc] init];
        event = [eventsArray objectAtIndex:indexPath.row];
        NSLog(@"COUNTRY NAME: %@",event.clubShort.country);
        NSLog(@"CLUB NAME: %@",event.clubShort.name_club);
        NSLog(@"EVENT ID: %@",event.id_event);
        cell = (EventCell*)[xibViews lastObject];
        if (event.fact.additional_charge) {
            cell.eventPrice.text = [NSString stringWithFormat:@"%@ €",event.fact.additional_charge];
            [cell.priceView setHidden:NO];
        }
        for (int i=0; i<eventsIdArray.count; i++) {
            NSString *storedStringEventId = [NSString stringWithFormat:@"%@",[eventsIdArray objectAtIndex:i]];
            NSString *realStringEventId = [NSString stringWithFormat:@"%@",event.id_event];
            if ([realStringEventId isEqualToString:storedStringEventId]) {
                cell.shareUrlString = [eventsUrlPageArray objectAtIndex:i];
                NSLog(@"EVENT ID: %@",[eventsIdArray objectAtIndex:i]);
                NSLog(@"EVENT URL: %@",[eventsUrlPageArray objectAtIndex:i]);
                break;
            }
        }
//        if([eventsIdArray containsObject:event.id_event])
//        {
//            
//        }
        [cell.eventLongText loadHTMLString:event.long_text baseURL:nil];
//        [cell.countryButton addTarget:self action:@selector(showWikiPopup:event.clubShort.country) forControlEvents:UIControlEventTouchUpInside];
        cell.eventName.text = event.name;
        cell.clubImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"event_%@.jpg",event.clubShort.id_club]];
        cell.flag_image.image = [UIImage imageNamed:[NSString stringWithFormat:@"flag_%@.png",event.clubShort.id_club]];
        [cell.eventImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.jpg",imgPath,event.id_event]]];
        cell.countryName = event.clubShort.country;
    }
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 768;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    CategoryEvent *event = [eventsCategoryArray objectAtIndex:indexPath.row];
//    EventsByCategoryViewController* viewController = [[EventsByCategoryViewController alloc] init];
//    viewController.categoryIdsList = event.categoryId;
//    viewController.startDate = self.startDate;
//    viewController.endDate = self.endDate;
//    [self.navigationController pushViewController:viewController animated:YES];
    //
}

//-(void)showWikiPopup:(NSString *)errorMessage{
-(void)showWikiPopup:(NSString *)country{
    WikiPopUp *statuspopup = [WikiPopUp loadWithNib];
    [statuspopup.closeButton addTarget:self action:@selector(dismissPopupView) forControlEvents:UIControlEventTouchUpInside];
    [statuspopup setTag:100];
    [statuspopup show];
}
- (void)dismissPopupView{
    
    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    
    if (!window) {
        window = ([UIApplication sharedApplication].windows)[0];
    }
    DialogView *currentPopup = (DialogView *)[window viewWithTag:100];
    [currentPopup cancel];
}
@end
