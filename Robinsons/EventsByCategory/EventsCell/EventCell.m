//
//  EventCell.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 28/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "EventCell.h"
#import "WikiPopUp.h"
#import <FacebookSDK/FacebookSDK.h>
@implementation EventCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)hideCountryView:(id)sender {

}
- (IBAction)showCountryView:(id)sender {
    [self showWikiPopup:self.countryName];
//    [self.InformationView setHidden:NO];
}


-(void)showWikiPopup:(NSString *)country{
    
    NSString *countryEncodedString =[[NSString alloc]
                                     initWithData:
                                     [country dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]
                                     encoding:NSASCIIStringEncoding];

    
    WikiPopUp *statuspopup = [WikiPopUp loadWithNib];
    NSLog(@"COUNTRY BEING RECEIVED: %@",country);
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://en.wikipedia.org/wiki/%@",countryEncodedString]]];
    NSLog(@"REQUEST FOR WIKI: %@",request);
    [statuspopup.webContentView loadRequest:request];
    [statuspopup.closeButton addTarget:self action:@selector(dismissPopupView) forControlEvents:UIControlEventTouchUpInside];
    [statuspopup setTag:100];
    [statuspopup show];
}
- (void)dismissPopupView{
    
    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    
    if (!window) {
        window = ([UIApplication sharedApplication].windows)[0];
    }
    DialogView *currentPopup = (DialogView *)[window viewWithTag:100];
    [currentPopup cancel];
}
- (IBAction)shareEventInformation:(id)sender {
    // Check if the Facebook app is installed and we can present
    // the message dialog
    // Check if the Facebook app is installed and we can present the share dialog
    FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
    params.link = [NSURL URLWithString:self.shareUrlString];
    
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        // Present the share dialog
        NSLog(@"native app present");
        
        // Present share dialog
        [FBDialogs presentShareDialogWithLink:params.link
                                      handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                          if(error) {
                                              // An error occurred, we need to handle the error
                                              // See: https://developers.facebook.com/docs/ios/errors
                                              NSLog(@"Error publishing story: %@", error.description);
                                          } else {
                                              // Success
                                              NSLog(@"result %@", results);
                                          }
                                      }];
        
    } else {
        // Present the feed dialog
        NSLog(@"use fallback plan");
    }
    
}

@end
