//
//  StatusPopUp.h
//  LoveScore
//
//  Created by Sarosh Salman Mirza on 16/12/14.
//  Copyright (c) 2014 saroshmirza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DialogView.h"
@interface WikiPopUp : DialogView
+ (id) loadWithNib;

@property (weak, nonatomic) IBOutlet UIView *webViewContainer;
@property (weak, nonatomic) IBOutlet UIWebView *webContentView;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@end
