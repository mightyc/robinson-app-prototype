//
//  StatusPopUp.m
//  LoveScore
//
//  Created by Sarosh Salman Mirza on 16/12/14.
//  Copyright (c) 2014 saroshmirza. All rights reserved.
//

#import "WikiPopUp.h"

@implementation WikiPopUp
+ (id) loadWithNib{
    
    WikiPopUp *result = (WikiPopUp *)[[NSBundle mainBundle] loadNibNamed:@"WikiPopUp" owner:nil options:nil][0];
    [result initializeSettings];
    
    return result;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
@end
