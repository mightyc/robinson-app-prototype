//
//  SelectTypeViewController.h
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 13/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectTypeViewController : UIViewController
@property (nonatomic, retain) NSString *startDate;
@property (nonatomic, retain) NSString *endDate;

@property(nonatomic, retain) NSString *seasonType;
@property (nonatomic, retain) NSString *seasonId;
@end
