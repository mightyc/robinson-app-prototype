//
//  SelectTypeViewController.m
//  Robinsons
//
//  Created by Sarosh Salman Mirza on 13/01/15.
//  Copyright (c) 2015 saroshmirza. All rights reserved.
//

#import "SelectTypeViewController.h"
#import "ClubCardViewController.h"
#import "SelectEventViewController.h"


@interface SelectTypeViewController ()

@end

@implementation SelectTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)selectTypeAction:(id)sender {
    
    UIButton *button = sender;
    if (button.tag == 1) {
        SelectEventViewController* viewController = [[SelectEventViewController alloc] init];
        viewController.seasonId = self.seasonId;
        viewController.startDate = self.startDate;
        viewController.endDate = self.endDate;
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
    else{
        ClubCardViewController* viewController = [[ClubCardViewController alloc] init];
        viewController.seasonId = self.seasonId;
        viewController.startDate = self.startDate;
        viewController.endDate = self.endDate;
        viewController.seasonType = self.seasonType;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
